using System;
using System.Web;

namespace SimplePasswordProtectHttpModule
{
    using System.Configuration;

    public class PasswordProtectHttpModule : IHttpModule
    {
        public String ModuleName
        {
            get { return "PasswordProtectHttpModule"; }
        }

        public void Init(HttpApplication application)
        {
            application.BeginRequest += ApplicationBeginRequest;
        }

        private static void ApplicationBeginRequest(Object source, EventArgs e)
        {
            var application = (HttpApplication)source;
            var request = application.Context.Request;
            var response = application.Context.Response;

            var cookie = request.Cookies["authenticated"];

            if (cookie != null && cookie.Value == "yes")
            {
                return;
            }

            var username = request.QueryString["username"];
            var password = request.QueryString["password"];

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                application.Response.Write("You do not have permission to view this page");
                application.Response.Flush();
                application.Response.End();
            }

            if (username == ConfigurationManager.AppSettings["username"] && password == ConfigurationManager.AppSettings["password"])
            {
                response.Cookies.Add(new HttpCookie("authenticated", "yes"));
            }
        }

        public void Dispose()
        {
        }
    }
}
